package com.fullcare.pathfinder.dtos;

import lombok.Data;

@Data
public class RegisterDto {

    private String name;
    private String surname;
    private String login;
    private String password;
    private String repeatPassword;
    private String email;
    private int phoneNumber;

}
