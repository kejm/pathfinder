package com.fullcare.pathfinder.dtos;

import lombok.Data;

@Data
public class ChangePasswordDto extends AbstractDto{

    private String currentPassword;
    private String newPassword;
    private String RepeatNewPassword;

}
