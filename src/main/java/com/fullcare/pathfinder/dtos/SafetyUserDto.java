package com.fullcare.pathfinder.dtos;

import lombok.Data;

@Data
public class SafetyUserDto extends AbstractDto {

    private String name;
    private String surname;

}
