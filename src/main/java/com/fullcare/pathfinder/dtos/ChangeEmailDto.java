package com.fullcare.pathfinder.dtos;

import lombok.Data;

@Data
public class ChangeEmailDto extends AbstractDto{

    private String newEmail;
    private String currentPassword;

}
