package com.fullcare.pathfinder.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class IncorrectEmailResponse {

    private String message;
    private String status;
    private String error;

}
