package com.fullcare.pathfinder.exceptions;

public class UserDontExist extends RuntimeException {
    public UserDontExist (String message) {
        super(message);
    }
}
