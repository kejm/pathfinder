package com.fullcare.pathfinder.exceptions;

public class AllreadyExistExceptions extends RuntimeException {
    public AllreadyExistExceptions(String message){
        super(message);
    }
}
