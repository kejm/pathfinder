package com.fullcare.pathfinder.exceptions.handlers;

import com.fullcare.pathfinder.dtos.responses.IncorrectEmailResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class ConstraintViolationExceptionHandler {

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<IncorrectEmailResponse> handleConstraintViolationException(Exception ex, WebRequest request) {
        return new ResponseEntity<IncorrectEmailResponse>(new IncorrectEmailResponse("406", "Not Acceptable", "Incorrect Email Syntax!"), new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE);
    }

}
