package com.fullcare.pathfinder.repositories;

import com.fullcare.pathfinder.entities.User;
import com.fullcare.pathfinder.projections.LogedUserProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    LogedUserProjection getUserById(Long id);

    List<User> getByEmailOrLogin(String email,String login);

}
