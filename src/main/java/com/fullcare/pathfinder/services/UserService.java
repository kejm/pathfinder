package com.fullcare.pathfinder.services;

import com.fullcare.pathfinder.entities.User;
import com.fullcare.pathfinder.projections.LogedUserProjection;
import com.fullcare.pathfinder.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAllUser(){
        return userRepository.findAll();
    }

    public LogedUserProjection getUserById(Long id){
        return userRepository.getUserById(id);
    }



}
