package com.fullcare.pathfinder.services;

import com.fullcare.pathfinder.dtos.ChangeEmailDto;
import com.fullcare.pathfinder.dtos.ChangePasswordDto;
import com.fullcare.pathfinder.dtos.RegisterDto;
import com.fullcare.pathfinder.entities.User;
import com.fullcare.pathfinder.exceptions.AllreadyExistExceptions;
import com.fullcare.pathfinder.exceptions.IncorrectPasswordException;
import com.fullcare.pathfinder.exceptions.UserDontExist;
import com.fullcare.pathfinder.mappers.UserMapper;
import com.fullcare.pathfinder.repositories.UserRepository;
import com.fullcare.pathfinder.services.encryptions.EncryptionService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    private UserRepository userRepository;
    private UserMapper userMapper;
    private EncryptionService encryptionService;

    @Autowired
    public AccountService(UserRepository userRepository, UserMapper userMapper, EncryptionService encryptionService) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.encryptionService = encryptionService;
    }

    public boolean passwordMatching(String password, String repeatPassword){
        return password.equals(repeatPassword);
    }

    public void register(RegisterDto registerDto){
        List<User> repeatingUser = userRepository.getByEmailOrLogin(registerDto.getEmail(), registerDto.getLogin());
        if(repeatingUser.isEmpty()){
            if(passwordMatching(registerDto.getPassword(), registerDto.getRepeatPassword())){
                User user = userMapper.toEntity(registerDto);
                user.setPassword(encryptionService.hashPassword(registerDto.getPassword()));
                userRepository.save(user);
            }else {
                throw new IncorrectPasswordException("Passwords aren't the same!");
            }
        }else {
            throw new AllreadyExistExceptions("This email or login is already use");
        }
    }

    public void changePassword(ChangePasswordDto changePasswordDto) throws NotFoundException {
        Optional<User> optionalUser = userRepository.findById(changePasswordDto.getId());
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            if(encryptionService.checkPassword(changePasswordDto.getCurrentPassword(), user.getPassword())){
                if(passwordMatching(changePasswordDto.getNewPassword(), changePasswordDto.getRepeatNewPassword())){
                    user.setPassword(encryptionService.hashPassword(changePasswordDto.getNewPassword()));
                    userRepository.save(user);
                }else {
                    throw new IncorrectPasswordException("Typed passwords aren't the same!");
                }
            }else {
                throw new IncorrectPasswordException("Wrong current password");
            }
        }else{
            throw new NotFoundException("User don't exist");
        }
    }

    public void changeEmail(ChangeEmailDto changeEmailDto){
        Optional<User> optionalUser = userRepository.findById(changeEmailDto.getId());
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            if(encryptionService.checkPassword(changeEmailDto.getCurrentPassword(), user.getPassword())){
                user.setEmail(changeEmailDto.getNewEmail());
                userRepository.save(user);
            }else{
                throw new IncorrectPasswordException("Typed passwords are not your password!");
            }
        }else{
            throw new UserDontExist("User don't exist");
        }
    }

}
