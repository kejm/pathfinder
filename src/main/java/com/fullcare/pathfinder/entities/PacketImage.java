package com.fullcare.pathfinder.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Entity
public class PacketImage extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "packet_id")
    private Packet packet;

    @ManyToOne
    @JoinColumn(name = "image_id")
    private Image image;
}
