package com.fullcare.pathfinder.entities;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class Packet extends AbstractEntity {

    private String name;
    private int durationTime;
    private float price;

}
