package com.fullcare.pathfinder.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Entity
public class User extends AbstractEntity {

    private String name;
    private String surname;
    private String login;
    private String password;
    private String email;
    private int phoneNumber;

    @ManyToOne
    @JoinColumn(name = "permission_id")
    private Permission permission;

}
