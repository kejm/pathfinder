package com.fullcare.pathfinder.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Entity
public class Order extends AbstractEntity{

    private long orderNumber;
    private float coast;
    private boolean status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
