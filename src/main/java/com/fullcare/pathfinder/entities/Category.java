package com.fullcare.pathfinder.entities;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class Category extends AbstractEntity {

    private String name;

}
