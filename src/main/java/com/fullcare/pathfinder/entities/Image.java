package com.fullcare.pathfinder.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@Entity
public class Image extends AbstractEntity {

    private String name;
    private String url;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

}
