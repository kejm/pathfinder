package com.fullcare.pathfinder.entities;

import lombok.Data;

import javax.persistence.Entity;

@Data
@Entity
public class Permission extends AbstractEntity {

    private String name;

}
