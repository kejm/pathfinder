package com.fullcare.pathfinder.projections;

public interface LogedUserProjection {

    String getName();
    String getSurname();
    String getLogin();
    String getEmail();
    int getPhoneNumber();



}
