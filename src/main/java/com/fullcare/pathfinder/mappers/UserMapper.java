package com.fullcare.pathfinder.mappers;

import com.fullcare.pathfinder.dtos.RegisterDto;
import com.fullcare.pathfinder.dtos.SafetyUserDto;
import com.fullcare.pathfinder.entities.User;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface UserMapper {

    SafetyUserDto toDto(User user);
    User toEntity(RegisterDto registerDto);

}
