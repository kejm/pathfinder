package com.fullcare.pathfinder.controllers;

import com.fullcare.pathfinder.dtos.ChangeEmailDto;
import com.fullcare.pathfinder.dtos.ChangePasswordDto;
import com.fullcare.pathfinder.dtos.RegisterDto;
import com.fullcare.pathfinder.services.AccountService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/acc")
public class AccountController {

    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping("/register")
    public void register(@RequestBody RegisterDto registerDto){
        accountService.register(registerDto);
    }

    @PutMapping("/change-password")
    public void changePassword(@RequestBody ChangePasswordDto changePasswordDto) throws NotFoundException {
        accountService.changePassword(changePasswordDto);
    }

    @PutMapping("/change-email")
    public void changeEmail(@RequestBody ChangeEmailDto changeEmailDto){
        accountService.changeEmail(changeEmailDto);
    }

}
